<div id="menu">
        <div>
            <h3>Installation Assistance</h3>
            <ul>
                <li>
                    <a ui-sref="login">Pre-Configure a new host.</a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Post-Configure a new host.</font></a>
                </li>
                <li>
                    <a ui-sref="">Upload ESD Client</a>
                </li>
                <li>
                	<a ui-sref="">Configure a Middle Tier.</a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Load SASAdmin users into an OMR</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Load JDK</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Install LSF</font></a>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
            </ul>
        </div>
        <div>
            <h3>Host Management</h3>
            <ul>
                <li>
                    <a ui-sref="">Renew/Update SAS Licenses</font></a>
                </li>
                <li>
                    <a ui-sref="">Renew/Update Dataflux License</font></a>
                </li>
                <li>
                    <a ui-sref="">Renew/Update LSF License</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Export and Import users</font></a>
                </li>
                <li>
                    <a ui-sref="">Precheck Host</a>
                </li>
                 <li>
                    <a ui-sref="land">Manage Host Services</a>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
           </ul>
        </div>
        <div>
            <h3>Restart Servers</h3>
            <ul>
                <li>
                    <a ui-sref="">Start/Stop SAS Servers</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Start/Stop Middle Tier</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Start/Stop LSF</font></a>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
            </ul>
        </div>
        <div>
            <h3>Maintenance</h3>
            <ul>
                <li>
                    <a ui-sref="><font color="gray">Install new order to depot</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Install Hot Fix</font></a>
                </li>
                <li>
                    <a ui-sref=""><font color="gray">Add License(s) to depot</font></a>
                </li>
                 <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
                <li>
                </li>
           </ul>
        </div>
</div>