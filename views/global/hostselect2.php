<div style="padding : 30px 10px 0px 10px;display:block;">
        <form>
            <div id="leftpanel" style="float:left;width:200px;padding:5px 15px;">
                <h3>Host List Filter</h3>
                <div>
                    <h4 style="margin-top: .3em;margin-bottom: .3em;width:180px;text-align:left;">Function</h4>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" data-ng-model="functions" name="type" value="MT" />Mid-Tier
                    </div>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" data-ng-model="functions" name="type" value="MOM" />MOM
                    </div>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" data-ng-model="functions" name="type" value="TS" />Terminal Server
                    </div>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" data-ng-model="functions" name="type" value="" />All
                    </div>
                </div>
                <hr />
                <div>
                    <h4 style="margin-top: .3em;margin-bottom: .3em;width:180px;text-align:left;">Validated/Non Validated</h4>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="validated" name="class" value="yes" />ODVS
                    </div>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="validated" name="class" value="no" />Non-Validate
                    </div>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="validated" name="class" value="" />All
                    </div>
                </div>
                <hr />
                <div>
                    <h4 style="margin-top: .3em;margin-bottom: .3em;width:180px;text-align:left;">Location</h4>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="location" name="location" value="Cary" />Cary
                    </div>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="location" name="location" value="uk9" />UK9
                    </div>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="location" name="location" value="ONYX" checked="" />Onyx
                    </div>
                    <div style="width:120px;text-align:left;">
                        <input type="radio" data-ng-model="location" name="location" value="" checked="" />All
                    </div>
                </div>
                <hr />
            </div>
            <div id="middlepanel" style="float:left;width:400px;padding:5px 15px;">
                <div style="width:160px;float:left;">
                    <b>Available Hosts:</b><br/>
                    <input type="text" data-ng-model="search" style="width:160px;padding:2px;margin-bottom:4px;" />
                    <select data-ng-model="selectedHost" data-ng-options="hosts.host for hosts in hostnames | filter:search |filter:functions | filter:validated | filter:location track by hosts.host" multiple="multiple" id="hostlist" style="width:160px;height:200px;">
                    </select>
                </div>
                <div style="float:left;width:50px;text-align:center;vertical-align:middle;padding-left:10px;padding-right:10px;padding-top:5px;margin-top:100px;">
                    <input type='button' id='btnRight' value ='  >  '/>
                    <br /><input type='button' id='btnLeft' value ='  <  '/>
                </div>
                <div style='width:160px;float:right'>
                    <b>Selected: </b><br/>
                    <select multiple="multiple" id='selected' style="width:160px;height:200px;margin-top:42px;">
                    </select>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div id="rightpanel" style="float:left;width:200px;padding:5px 15px;">
                <h3>Mode</h3>
                <div>
                    <h4 style="margin-top: .3em;margin-bottom: .3em;;width:180px;text-align:right;">Start/Stop/Restart</h4>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" name="mode" value="restart" />Look for SAS Scripts
                    </div>
                </div>
                <hr />
                <div>
                    <h4 style="margin-top: .3em;margin-bottom: .3em;;width:180px;text-align:right;">Mode</h4>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" name="mode" value="detect" />Detect
                    </div>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" name="mode" value="harden" />Harden
                    </div>
                </div>
                <hr />
                <div>
                    <h4 style="margin-top: .3em;margin-bottom: .3em;width:180px;text-align:right;">No Action</h4>
                    <div style="width:180px;text-align:left;">
                        <input type="radio" name="mode" value="" checked="" />None
                    </div>
                </div>
                <hr />
            </div>
            <div style="clear:left;"></div>
            <div id="buttonholder">
                <button id="start" name="start" class="action bluebtn" tabindex="3"><span class="label">Start</span></button>
            </div>
        </form>
</div>