﻿$(document).ready(function () {

var username = $('input#username').val();
	
	if(localStorage.getItem("hostname") !== null) {
		$('input#host').val(localStorage.hostname);
	}
	
	$('input#host').blur(function() {
		if($('input#host').val() != '') {
			localStorage.hostname = $('input#host').val();
		}
	});
	
	if(localStorage.getItem("project") !== null) {
		$('input#projid').val(localStorage.project);
	}
	
	$('input#projid').blur(function() {
		if($('input#projid').val() != '') {
			localStorage.project = $('input#projid').val();
		}
	});

	/*///////////////////////
	/////Precheck Script/////
	///////////////////////*/
	
	$('button#start').bind('click', function(e){
		
		/*////////////////////////////////////////////
		/////lets validate data on the login form/////
		////////////////////////////////////////////*/
		
		var reason = '';
		/*reason += validateHost(this.form.host);
		reason += validateProject(this.form.projid);*/
		
		/*//////////////////////////////////////////////////
		/////if there is a reason the form is not valid///// 
		/////then we will send a popup with the reason /////
		//////////////////////////////////////////////////*/
		
		if (reason != "") {
			var x = document.getElementById('formAlert');
			x.innerHTML = reason;
			$('#linkAlert').click();
			
			/*////////////////////////////////////////////////
			/////The data in the form appears to be valid/////
			////////////////////////////////////////////////*/

			} else {
			
			/*//////////////////////////////////
			/////Preventing a double submit/////
			//////////////////////////////////*/
					
			$('button#start').prop('disabled', 'true');
			var proj = $('input#projid').val();
			var projid = proj.toLowerCase();
			var host = $('input#host').val();
			$('div#buttonholder').html('');
			$('div#dyncontent').html('');
			$('h3#prck').html('Pre-check List for '+host);
			
			/*/////////////////////////
			/////Validate cidowner/////
			/////////////////////////*/
		
			$.ajax({
				type: 'POST',
				url: '/SSoD-0311',
				data: {
					projid : projid,
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while '+projid+'owner<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){
					
					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/
					
					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/
					
					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated '+projid+'owner.<font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure.</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for '+projid+'owner failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '04':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '05':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;                                                    
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					cidrun(projid, host);
				}
			})
		}
		
		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/
		
		e.preventDefault();
	});
	function cidrun(projid, host) {
				
		/*///////////////////////
		/////Validate CIDRun/////
		///////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0312',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while '+projid+'run<br />is validated');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated '+projid+'run<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for '+projid+'run failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;                                                   
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;                                                
				}
			},
			complete: function(){
				$('#prepage').hide();
				ciddemo(projid, host);
			}
		});
	}
	function ciddemo(projid, host) {
				
		/*////////////////////////
		/////Validate CIDDemo/////
		////////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0313',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while '+projid+'demo<br />is validated');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated '+projid+'demo<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for '+projid+'demo failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				cidsrv(projid, host)
			}
		});
	}
	function cidsrv(projid, host) {

		/*///////////////////////
		/////Validate CIDSrv/////
		///////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0314',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while '+projid+'srv<br />is validated');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated '+projid+'srv<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for '+projid+'srv failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				sudo(projid, host);
			}
		});
	}
	function sudo(projid, host) {
			
		/*///////////////////
		/////SUDO Access/////
		///////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0315',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while SUDO using '+username+'<br />is validated');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">SUDO Access Confirmed using '+username+'<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to verify SUDO access using '+username+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				ulimit(projid, host);
			}
		});
	}
	function ulimit(projid, host) {
		
		/*////////////////////
		/////Ulimit check/////
		////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0316',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while unix ulimit<br />is validated');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Ulimit has been set to 20480 or higher<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Ulimit is set too low</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				admindir(projid, host)
			}
		});
	}
	function admindir(projid, host) {
			
		/*/////////////////////////////
		/////Admin Directory check/////
		/////////////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0317',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,	
			beforeSend: function() {
				$('div#pptxt').html('Please wait while SVN admin <br />is located');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Found the Admin directory<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot verify the admin directory</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				ssorun(projid, host);
			}
		});
	}
	function ssorun(projid, host) {
				
		/*//////////////////////////////
		/////Common Directory check/////
		//////////////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0318',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,	
			beforeSend: function() {
				$('div#pptxt').html('Please wait while SSO_RUN <br />is located');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">SSO_RUN has been installed<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for SSO_RUN installation failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				ssodirperms(projid, host);
			}
		});
	}
	function ssodirperms(projid, host) {
				
		/*///////////////////////////
		/////SSO Directory check/////
		///////////////////////////*/

		return $.ajax({
			type: 'POST',
			url: '/SSoD-0319',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while /sso directory <br />premissions are checked');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">SSO Directory Permissions confirmed<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for SSO Directory permissions failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);								break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				esdclient(projid, host);
			}
		});
	}
	function esdclient(projid, host) {
				
		/*////////////////////////
		/////ESD Client check/////
		////////////////////////*/

		$.ajax({
			type: 'POST',
			url: '/SSoD-0320',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait while esd client <br />is detected');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">ESD Client found in thirdparty directory<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for ESD Client failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				javainstalled(projid, host);
			}
		});
	}
	function javainstalled(projid, host) {
				
		/*//////////////////////
		/////Java Installed/////
		//////////////////////*/

		return $.ajax({
			type: 'POST',
			url: '/SSoD-0321',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,	
			beforeSend: function() {
				$('div#pptxt').html('Please wait ... <br />Looking for JDK');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Java installation confirmed<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for JDK failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				junitinstalled(projid, host);
			}
		});
	}
	function junitinstalled(projid, host) {
			
		/*///////////////////////
		/////JUnit Installed/////
		///////////////////////*/

		return $.ajax({
			type: 'POST',
			url: '/SSoD-0322',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,	
			beforeSend: function() {
				$('div#pptxt').html('Please wait ... <br />Looking for JUnit');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">JUnit is installed<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for JUnit failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function(){
				$('#prepage').hide();
				jbossinstalled(projid, host);
			}
		});
	}
	function jbossinstalled(projid, host) {
			
		/*///////////////////
		/////JBoss check/////
		///////////////////*/

		return $.ajax({
			type: 'POST',
			url: '/SSoD-0323',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait ... <br />Looking for JBoss');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">JBoss is installed<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for JBoss failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function() {
				$('#prepage').hide();
				checkhostname(projid, host);
			}
		});
	}
	function checkhostname(projid, host) {
			
		/*///////////////////
		/////JBoss check/////
		///////////////////*/

		return $.ajax({
			type: 'POST',
			url: '/SSoD-0324',
			data: {
				projid : projid,
				host : host,
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {
				$('div#pptxt').html('Please wait ... <br />Checking the hostname for '+host);
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						/*update content with success*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">The Hostname has been verified<font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '02':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Login Failure</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
					case '03':
						/*update content with failure*/
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Validation for Hostname failed</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
                                        case '04':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Cannot find host '+host+'.</font></div>';
                                                $('div#dyncontent').append(myhtml);
                                                break;
                                        case '05':
                                                /*update content with failure*/
                                                myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
                                                $('div#dyncontent').append(myhtml);
					default:
						myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
						$('div#dyncontent').append(myhtml);
						break;
				}
			},
			complete: function() {
				$('#prepage').hide();
				$('div#pptxt').html('');
				$('div#buttonholder').append('<button id="again" name="again" class="action bluebtn"><span class="label">Check another host</span></button>');
			}
		});
	}
	$('button#again').livequery("click", function () {
		window.top.location.href = '/SSoD-0310';
	});
});			
			