﻿<!-- Verify user and load header -->
<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Pre-Check Host', 'data-ng-app="precheck"');
endif;
?>
<!-- Custom CSS and Javascript -->
<link rel="stylesheet" href="/SSoD-CSS0005" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0310"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
<div id="page" name="page" data-ng-controller="PageController">
			<div id="header" name="header">
				<a name="home" id="home" href="javascript:void(0);"tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" tabindex="10">Logout</a>
				<h2>ZIPPER</h2>
			</div>
    <div id="content" data-ng-controller="ContentController">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div style="width:475px;border-style:solid;padding: 4px 4px 20px 4px;border-width:1px;border-color:blue;margin:0px auto;">
				<h3 id="prck" id="name">Precheck</h3>
				<form style="width:410px;margin: 0px auto;">
					<div id="dyncontent">
						<div>
							<?php
							if(isset($_SESSION['zipper']['hostmachine'] -> machine) && $_SESSION['zipper']['hostmachine'] -> machine != '') :
								echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> machine.'" autocomplete="off" />';
							else:
								echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" autocomplete="off" />';
							endif;
							?>
						</div>
						<div style="float:left;">
							<?php
							if(isset($_SESSION['zipper']['hostmachine'] -> projectid) && $_SESSION['zipper']['hostmachine'] -> projectid != '') :
								echo '<input id="projid" name="projid" class="project config" placeholder="Project ID" type="text" tabindex="2" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> projectid.'" autocomplete="off" maxlength="5" />';
							else:
								echo '<input id="projid" name="projid" class="project config" placeholder="Project ID" type="text" tabindex="2" require="required" autocomplete="off" maxlength="5" />';
							endif;
							?>
						</div>
						<div style="clear:left;"></div>
						<div>
						<input id="username" name="username" type="hidden" value="<?php echo $_SESSION['zipper']['zipper'] -> username ?>"  />
						</div>
					</div>
					<div id="buttonholder">
						<button id="start" name="start" class="action bluebtn" tabindex="3"><span class="label">Start</span></button>
					</div>
				</form>
			</div>
		</div>
                <?php echo $_SESSION['zipper']['zipper'] -> footer(); ?>
	</div>
</div>
</body>
</html>

