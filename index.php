<!DOCTYPE html>
<!-- define angular app -->
<html data-ng-app="bridge" lang="en">
	<head>
		<meta charset="utf-8" />
	        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Bridge</title>
		<!-- <base href="/bridge/></base> -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="./css/main.css" type="text/css" media="screen" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.10/angular-ui-router.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>
		<script src="./js/main.js"></script>
		<script src="./js/router.js"></script>
	</head>
	<body  onLoad="waitPreloadPage();">
		<div id="prepage" name="prepage">
			<div id="pptxt" name="pptxt">Bridge is Loading</div>
			<div><img src="./images/preloader.gif" class="prepage"></div>
		</div>
		<div id="wrapper">
			<div id="page" name="page">
				<header>
					<data-ng-include src="'./views/global/header.php'"></data-ng-include>
				</header>
				<div id="content">
					<!-- angular templating -->
					<!-- this is where content will be injected -->
					<div data-ui-view></div>
				</div>
				<footer>
					<data-ng-include src="'./views/global/footer.php'"><data-ng-include>
				</footer>
			</div>
		</div>
	</body>
</html>
