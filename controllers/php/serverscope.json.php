<?php
header('Content-Type: application/json');
$hostArray = array();
$mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
if(mysqli_connect_errno()):
    $mysqli -> close();
    while(@ob_end_flush());
    exit;
else :
    if($stmt = $mysqli->prepare("SELECT DISTINCT host, function, validated, location FROM zipper_xymon WHERE managed = 'yes';")):
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($host, $function, $validated, $location);
    else:
        exit;
    endif;
endif;
while($stmt->fetch()){
    array_push($hostArray, '{"host": "'.$host.'", "function": "'.$function.'","validated": "'.$validated.'","location": "'.$location.'"}');
}
$result = str_replace("'", "",json_encode($hostArray)).';';
$result = str_replace("\\", "", $result);
$result = str_replace('}"', '}', $result);
$result = str_replace('"{', '{', $result);
echo $result;
?>