<div style="display:block; width:750px;padding : 20px;margin: 0px auto;border: 1px solid gray;">
	<h3>Choose a method to select your host(s):</h3>
	<tabset justified="true">
		<tab heading="Single Host"><data-ng-include src="'./views/global/hostselect1.php'"></data-ng-include></tab>
		<tab heading="Search Host(s)"><data-ng-include src="'./views/global/hostselect2.php'"></data-ng-include></tab>
		<tab heading="Upload Host(s)"><data-ng-include src="'./views/global/hostselect3.php'"></data-ng-include></tab>
	</tabset>
<div>
<hr />
<data-ng-include src="'./views/global/actions.php'"></data-ng-include>

